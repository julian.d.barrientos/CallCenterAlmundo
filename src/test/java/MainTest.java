import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import org.junit.Assert;
import objects.Dispatcher;
import objects.Empleado;
import objects.ITipoEmpleado;
import objects.Ventana;

public class MainTest {

	 @Test
	 public void testFor10Calls() {
		 List<Empleado> empleados = new ArrayList<>();
		 Dispatcher dispatcher = new Dispatcher();
		 for (int i = 0; i < 11; i++) {
			empleados.add(new Empleado(ITipoEmpleado.Operador));
		 }
		 for (int i = 0; i < 12; i++) {
			 if(i==10) 	
				Assert.assertEquals(200,dispatcher.dispatchCall(empleados));
			else 
				Assert.assertEquals(300,dispatcher.dispatchCall(empleados));
		 }
		
	 }
}
