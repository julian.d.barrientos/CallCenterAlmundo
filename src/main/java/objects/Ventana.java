package objects;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * Clase Ventana
 * Muestra la estructuta que deberia tener una Ventana en Java con la libreria
 * Swing, contiene una etiqueta, un caja de texto y un boton, que tiene la
 * accion de mostrar el texto en la caja por una ventana de mensaje.
 * @author Daniel Alvarez (a3dany)
 */
public class Ventana extends JFrame implements ActionListener {
   
	private JButton boton;          // boton con una determinada accion
    Dispatcher dispt;
    List<Empleado> empleados;
    public Ventana() {
        super();                    // usamos el contructor de la clase padre JFrame
        this.dispt = new Dispatcher(); //inicializamos el dispatcher
        this.empleados = new ArrayList<>();
        //agregamos los empleados a la lista
        this.empleados.add(new Empleado(ITipoEmpleado.Operador));
        this.empleados.add(new Empleado(ITipoEmpleado.Operador));
        this.empleados.add(new Empleado(ITipoEmpleado.Operador));
        this.empleados.add(new Empleado(ITipoEmpleado.Operador));
        this.empleados.add(new Empleado(ITipoEmpleado.Supervisor));
        this.empleados.add(new Empleado(ITipoEmpleado.Supervisor));
        this.empleados.add(new Empleado(ITipoEmpleado.Director));
        configurarVentana();        // configuramos la ventana
        inicializarComponentes();   // inicializamos los atributos o componentes
    }

    private void configurarVentana() {
        this.setTitle("Esta Es Una Ventana");                   // colocamos titulo a la ventana
        this.setSize(310, 210);                                 // colocamos tamanio a la ventana (ancho, alto)
        this.setLocationRelativeTo(null);                       // centramos la ventana en la pantalla
        this.setLayout(null);                                   // no usamos ningun layout, solo asi podremos dar posiciones a los componentes
        this.setResizable(false);                               // hacemos que la ventana no sea redimiensionable
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    // hacemos que cuando se cierre la ventana termina todo proceso
    }

    private void inicializarComponentes() {
        // creamos los componentes
        boton = new JButton();
        // configuramos los componentes
        boton.setText("Call");   // colocamos un texto al boton
        boton.setBounds(50, 100, 200, 30);  // colocamos posicion y tamanio al boton (x, y, ancho, alto)
        boton.addActionListener(this);      // hacemos que el boton tenga una accion y esa accion estara en esta clase
        // adicionamos los componentes a la ventana
        this.add(boton);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
		int message = dispt.dispatchCall(this.empleados); //realizo la llamada
		//resuelvo el mensaje a mostrar dependiendo del resultado
		switch (message) {
		case 100 :
			JOptionPane.showMessageDialog(this, "Todos los empleados estan ocupados, vuelva  a intentar mas tarde");
			break;
		case 200:
			JOptionPane.showMessageDialog(this, "Todos las lineas estan ocupadas, intente de nuevo mas tarde");
			break;
		case 300:
			JOptionPane.showMessageDialog(this, "Llamando");
			break;
		}
		
    }
}
