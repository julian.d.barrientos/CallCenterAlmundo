package objects;

import java.awt.Component;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.hamcrest.core.Is;
import org.omg.CosNaming.IstringHelper;

public class Dispatcher {
	private List<AsyncTaskCall> _calls;
	
	public Dispatcher() {
		//al inicializar el objeto genero las posibles llamads asyncronicas
		this._calls = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			this._calls.add(new AsyncTaskCall());
		}
	}
	public int dispatchCall (List<Empleado> listEmp, Object...args) {
		boolean busy = true;
		// verifico que haya empleados libres con la jerarquia pedida 
		Empleado empleado = getEpleadoFree(listEmp, ITipoEmpleado.Operador);
		if(empleado == null) {
			empleado = getEpleadoFree(listEmp, ITipoEmpleado.Supervisor);
			if(empleado == null){
				empleado = getEpleadoFree(listEmp, ITipoEmpleado.Director);
				if(empleado == null){
					return 100;
				}
			}
		}
		for (AsyncTaskCall call : this._calls) {
			// si hay empleados libres se llega aca y busca si hay llamadas disponibles
			if(!call.isInUse()) {
				call.executeEvent(empleado, args);
				call.setInUse(true);
				busy = false;
				break;
			}
		}
		if(busy)
		{
			return 200; 
		}
		return 300;
	}
	private Empleado getEpleadoFree(List<Empleado> listEmp, ITipoEmpleado tipoEmpleado) {
		Empleado empleado = null;
		for (Empleado object : listEmp) {
			if(object.getTipoEmpleado().equals(tipoEmpleado) && object.isFree()) {
				empleado = object;
				break;
			}
		}
		return empleado;
	}
	
}
