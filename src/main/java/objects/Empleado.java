package objects;

public class Empleado {
	private ITipoEmpleado _tipoEmpleado;
	private boolean _free;

	public Empleado () {
		this.setTipoEmpleado(ITipoEmpleado.Operador);
		this.setFree(true);
	}
	
	public Empleado (ITipoEmpleado tipoEmpleado, boolean free) {
		this.setTipoEmpleado(tipoEmpleado);
		this.setFree(free);
	}
	public Empleado(ITipoEmpleado tipoEmpleado) {
		this(tipoEmpleado,true);
	}
	
	
	public ITipoEmpleado getTipoEmpleado() {
		return _tipoEmpleado;
	}

	public void setTipoEmpleado(ITipoEmpleado _tipoEmpleado) {
		this._tipoEmpleado = _tipoEmpleado;
	}
	
	public boolean isFree() {
		return _free;
	}
	
	public void setFree(boolean _notFree) {
		this._free = _notFree;
	}
	
}
