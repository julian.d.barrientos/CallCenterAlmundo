package objects;

import java.io.Console;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class AsyncTaskCall {
	private Thread t;
	private boolean _inUse;

	int Low = 5;
	int High = 10;
	
	public AsyncTaskCall() {
		this.setInUse(false);
	}
	
	
	public void executeEvent(Empleado object, Object... args) {
		
	    this.t = new Thread(new Runnable() {
	        @Override
	        public void run() {
	            try {
	            	object.setFree(false);
	        		Timer timer = new Timer();
	        		Random r = new Random();
	        		int seconds = r.nextInt(High-Low) + Low;
	        		System.out.println("Enter thread for "+ seconds +" seconds with "+ object.getTipoEmpleado().toString());
	        		timer.schedule(new TimerTask() {
	        			  @Override
	        			  public void run() {
	        			    object.setFree(true);
	        			    AsyncTaskCall.this.setInUse(false);
	        			    System.out.println("End of thread for "+ object.getTipoEmpleado().toString());
	        			  }
	        			}, seconds * 1000);
	            } catch (Exception ex) {
	              //  logger.error(ex.getMessage(), ex);
	            }
	        }
	    });
	    this.t.start();
	}


	public boolean isInUse() {
		return _inUse;
	}


	public void setInUse(boolean _inUse) {
		this._inUse = _inUse;
	}
}
